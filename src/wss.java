import java.io.*;
public class wss {

	public static void main(String[] args) {
		//initialization of variables 
		int height,width,wordN;
		char[][] grid = null;
	    String[] words = null;
	    String inputPath = args[0];
		String outputPath = args[1];
	    
		try {// try/catch for IOException handling
			FileReader input = new FileReader(inputPath);
			BufferedReader br = new BufferedReader(input);
			
			//*****reading data from input*****//
		    String line = br.readLine();
		    height = Integer.parseInt(line.split(",")[0]);
		    width = Integer.parseInt(line.split(",")[1]);
		    grid = new char[height][width];
		    
		    for(int i = 0; i < height; i++) {
		    	line = br.readLine();
		        String[] vrstica = line.split(",");
		    	for(int j = 0; j < width; j++)
		    		grid[i][j] = vrstica[j].charAt(0);
		    }
		    
		    line = br.readLine();
		    wordN = Integer.parseInt(line);
		    words = new String[wordN];
		    for(int i = 0; i < wordN; i++) {
		    	words[i] = br.readLine();
		    }
		    br.close();
		    input.close();
		    //*****end of reading input*****//
		    
		    //sort words array length for efficiency
		    MergeSort sorter = new MergeSort(); 
	        sorter.sort(words, 0, words.length-1, true);
	        
	        PrintWriter writer = new PrintWriter(outputPath, "UTF-8");
			writer.println("word, (x,y), direction");
			
			// initial call of recursion - start with longest word
			search(words,grid,words.length-1,writer);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**Method to search for words in a grid of letters, if successful prints results into output file in the format: "word,(start_x,start_y),[direction vector]"
	 * @param words  array of Strings ordered by length in ascending order
	 * @param grid  2D array of characters in which all the words can be found
	 * @param word_i  index of current word to be found
	 * @param writer  PrintWriter stream to output file
	 * @return True if the Strings in String[] words from word_i to 0 can be found on char[][] grid without overlapping
	**/
	public static boolean search(String[] words, char[][] grid, int word_i, PrintWriter writer) {
		if(word_i==-1) return true;// return success if we made it through all the words
		
		//for loops to search through the whole grid
		for(int y = 0; y < grid.length; y++ ) {
			for(int x = 0; x < grid[0].length; x++ ) {
				if(words[word_i].charAt(0) != grid[y][x]) continue;// if the first letter is wrong already then don't even look here
				
				//for loop that rotates the direction and checks for the word at the point (x,y)
				Vector2D v = new Vector2D(1,0);
				for(int k = 0; k < 8; k++) {
					//check if the word can possibly fit here and if yes try to fit it in
					if( canFit(grid,words[word_i],x,y,v) && tryFit(grid,words[word_i],x,y,v) ) {
						//if it fits, remove used up characters from the grid to ensure no overlapping
						for(int l = 0; l < words[word_i].length(); l++) {
							grid[y + l*(int)v.y][x + l*(int)v.x] = 0;
						}
						//recursive call ~ see if the rest of the words can fit in the grid
						if(search(words,grid,word_i-1,writer)) {
							writer.println(words[word_i] +",(" + x +","+ y + "),"+v);
							return true;
						}
						//fill the removed characters back in - ONLY happens if the rest of the words couldn't fit in this way
						for(int l = 0; l < words[word_i].length(); l++) {
							grid[y + l*(int)v.y][x + l*(int)v.x] = words[word_i].charAt(l);
						}
					}
					//rotate the direction vector by 45 degrees and round components to nearest int
					v.rotate(45);
					v.toInt();
				}
			}
		}
		return false;
	}
	
	//Method to check if word can possibly fit in the given direction
	public static boolean canFit(char[][] grid, String word, int x, int y, Vector2D v) {
		int _x = (int) (x + v.x*(word.length()-1));
		int _y = (int) (y + v.y*(word.length()-1));
		//check if end points of the word would be out of bounds
		return (_x > -1 && _x < grid[0].length) && (_y > -1 && _y < grid.length);
	}
	
	/**Method to check if word fits in the given direction, starting at position grid[y][x]*/
	public static boolean tryFit(char[][] grid, String word, int x, int y, Vector2D v) {
		//start with 1 since we already know first elements fits
		for(int i = 1; i < word.length(); i++) {
			x += v.x;
			y += v.y;
			if(word.charAt(i) != grid[y][x]) return false;
		}
		return true;
	}
}
